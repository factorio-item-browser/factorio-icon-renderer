# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v1.0.4] - 2024-11-23
### Fixed
- Optional icon size of Factorio 2.x now defaults to 64.

## [v1.0.2] - 2024-05-19
### Fixed
- Scaling issues with technologies. An additional parameter has to be passed to `NewRenderIcon()` to accommodate for it.

## [v1.0.1] - 2024-05-01
### Fixed
- Layers without their own size did not inherit the size of the first layer.
### Changed
- Upgraded project to Go 1.22.

## [v1.0.0] - 2023-09-28
### Added
- Initial release of the package.

[Unreleased]: https://gitlab.com/factorio-item-browser/factorio-icon-renderer/-/compare/v1.0.4...HEAD
[v1.0.4]: https://gitlab.com/factorio-item-browser/factorio-icon-renderer/-/compare/v1.0.2...v1.0.4
[v1.0.2]: https://gitlab.com/factorio-item-browser/factorio-icon-renderer/-/compare/v1.0.1...v1.0.2
[v1.0.1]: https://gitlab.com/factorio-item-browser/factorio-icon-renderer/-/compare/v1.0.0...v1.0.1
[v1.0.0]: https://gitlab.com/factorio-item-browser/factorio-icon-renderer/-/tags/v1.0.0
