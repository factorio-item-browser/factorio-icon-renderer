# Factorio Icon Renderer

A Go package helping with stitching together the layers of icons from Factorio.

## Usage

This package requires the raw icon definition from the Factorio game. It does not support the lua syntax of the icons,
but it is possible to export the data from the game in a JSON format, and unmarshal it to use with this package.

See the following example to use the renderer:

```go
package main

import (
	"image/png"
	"os"

	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/renderer"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/transfer"
)

func main() {
	// The icon with all its data from the game. Use e.g. json.Unmarshal() to fill in the data from a JSON dump.
	var icon transfer.FactorioIcon

	// Initialize the renderer. You must provide the data and the mods directory. The package supports both, still 
	// zipped mods (in case of the mods directory) or already extracted ones (in case of the data directory). 
	r := renderer.New()
	r.AddModsDirectory("/opt/factorio/data")
	r.AddModsDirectory("/opt/factorio/mods")

	// Render the icon. The 64 specifies the final size in which the icon should be output.
	img, err := r.Render(transfer.NewRenderIcon(icon, transfer.ScaleBaseItemOrRecipe, 64))
	if err != nil {
		panic(err)
	}

	// Write image to the disk using any encoder.
	out, err := os.Create("test.png")
	if err != nil {
		panic(err)
	}

	err = png.Encode(out, img)
	if err != nil {
		panic(err)
	}
}
```
