package main

import (
	_ "embed"
	"encoding/json"
	"image/png"
	"os"

	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/renderer"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/transfer"
)

//go:embed icon.json
var iconData []byte

func main() {
	// Unmarshal the JSON data exported from the game
	var icon transfer.FactorioIcon
	_ = json.Unmarshal(iconData, &icon)

	// Initialize the renderer
	r := renderer.New()
	r.AddModsDirectory("/opt/factorio/data")
	r.AddModsDirectory("/opt/factorio/mods")

	// Render the icon
	img, err := r.Render(transfer.NewRenderIcon(icon, transfer.ScaleBaseItemOrRecipe, 64))
	if err != nil {
		panic(err)
	}

	// Write image to the disk
	out, err := os.Create("test.png")
	if err != nil {
		panic(err)
	}

	err = png.Encode(out, img)
	if err != nil {
		panic(err)
	}
}
