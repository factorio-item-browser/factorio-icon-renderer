package errors

import "fmt"

// InvalidImageFileNameError is returned when a filename of an image could not be interpreted.
type InvalidImageFileNameError struct {
	fileName string
}

func NewInvalidImageFileNameError(fileName string) *InvalidImageFileNameError {
	return &InvalidImageFileNameError{
		fileName: fileName,
	}
}

func (e *InvalidImageFileNameError) Error() string {
	return fmt.Sprintf("invalid image filename: %s", e.fileName)
}

// UnknownModError is returned when the loader encounters an unknown mod.
type UnknownModError struct {
	modName string
}

func NewUnknownModError(modName string) *UnknownModError {
	return &UnknownModError{
		modName: modName,
	}
}

func (e *UnknownModError) Error() string {
	return fmt.Sprintf("unknown mod: %s", e.modName)
}

// LoadImageError is returned when an image was unable to be loaded through the loader.
type LoadImageError struct {
	modName       string
	imagePath     string
	originalError error
}

func NewLoadImageError(modName, imagePath string, originalError error) *LoadImageError {
	return &LoadImageError{
		modName:       modName,
		imagePath:     imagePath,
		originalError: originalError,
	}
}

func (e *LoadImageError) Error() string {
	return fmt.Sprintf("load image error: image %s from mod %s: %s", e.imagePath, e.modName, e.originalError)
}

func (e *LoadImageError) Unwrap() error {
	return e.originalError
}
