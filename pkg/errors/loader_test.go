package errors

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestInvalidImageFileNameError(t *testing.T) {
	fileName := "abc"
	expectedMessage := "invalid image filename: abc"

	instance := NewInvalidImageFileNameError(fileName)

	assert.NotNil(t, instance)
	assert.Equal(t, expectedMessage, instance.Error())
}

func TestUnknownModError(t *testing.T) {
	modName := "abc"
	expectedMessage := "unknown mod: abc"

	instance := NewUnknownModError(modName)

	assert.NotNil(t, instance)
	assert.Equal(t, expectedMessage, instance.Error())
}

func TestLoadImageError(t *testing.T) {
	modName := "abc"
	imagePath := "def"
	originalError := fmt.Errorf("test error")
	expectedMessage := "load image error: image def from mod abc: test error"

	instance := NewLoadImageError(modName, imagePath, originalError)

	assert.NotNil(t, instance)
	assert.Equal(t, expectedMessage, instance.Error())
	assert.Equal(t, originalError, instance.Unwrap())
}
