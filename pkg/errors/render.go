package errors

import (
	"fmt"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/transfer"
)

// RenderError is the error wrapper returned by the renderer when any underlying error occurs while rendering an icon.
type RenderError struct {
	icon          transfer.RenderIcon
	originalError error
}

func NewRenderError(icon transfer.RenderIcon, originalError error) *RenderError {
	return &RenderError{
		icon:          icon,
		originalError: originalError,
	}
}

func (e *RenderError) Error() string {
	return fmt.Sprintf("render error: %s", e.originalError)
}

func (e *RenderError) Unwrap() error {
	return e.originalError
}

func (e *RenderError) Icon() transfer.RenderIcon {
	return e.icon
}
