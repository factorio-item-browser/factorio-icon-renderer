package errors

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/transfer"
	"testing"
)

func TestRenderError(t *testing.T) {
	icon := transfer.RenderIcon{RenderedSize: 42}
	originalError := fmt.Errorf("test error")
	expectedMessage := "render error: test error"

	instance := NewRenderError(icon, originalError)

	assert.NotNil(t, instance)
	assert.Equal(t, expectedMessage, instance.Error())
	assert.Equal(t, originalError, instance.Unwrap())
	assert.Equal(t, icon, instance.Icon())
}
