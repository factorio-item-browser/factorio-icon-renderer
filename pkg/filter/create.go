package filter

import "image"

// CreateFilter is a filter which creates a new (empty) image with the provided size.
type CreateFilter = func(size int) image.Image

// Create creates a new RGBA image with the provided size.
func Create(size int) image.Image {
	return image.NewRGBA(image.Rect(0, 0, size, size))
}
