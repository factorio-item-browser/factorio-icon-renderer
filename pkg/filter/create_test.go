package filter

import (
	"github.com/stretchr/testify/assert"
	"image"
	"testing"
)

func TestCreate(t *testing.T) {
	size := 2

	expectedResult := image.RGBA{
		Pix: []uint8{
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		},
		Stride: 8,
		Rect:   image.Rect(0, 0, 2, 2),
	}

	result := Create(size)

	assert.Equal(t, &expectedResult, result)
}
