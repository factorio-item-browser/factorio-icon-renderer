package filter

import (
	"github.com/anthonynsimon/bild/clone"
	"github.com/anthonynsimon/bild/transform"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/transfer"
	"image"
	"math"
)

var resizeAlgorithm = transform.MitchellNetravali

// LayerFilter manipulates the current layer image of the icon.
type LayerFilter func(icon transfer.RenderIcon, source image.Image, layer transfer.Layer) image.Image

// ChainLayerFilters creates a new LayerFilter by chaining together the provided ones.
func ChainLayerFilters(layerFilters ...LayerFilter) LayerFilter {
	return func(icon transfer.RenderIcon, source image.Image, layer transfer.Layer) image.Image {
		for _, filter := range layerFilters {
			source = filter(icon, source, layer)
		}
		return source
	}
}

// RemoveMipMaps is the filter for removing the mip maps of the source, only keeping the highest resolution of it.
func RemoveMipMaps(_ transfer.RenderIcon, source image.Image, layer transfer.Layer) image.Image {
	return transform.Crop(source, image.Rect(0, 0, layer.Size, layer.Size))
}

// Scale applies the scaling value of the layer to the source.
func Scale(icon transfer.RenderIcon, source image.Image, layer transfer.Layer) image.Image {
	scale := layer.Scale * icon.RenderedScale
	if scale == 1. {
		return source
	}

	newSize := int(math.Round(float64(layer.Size) * scale))
	return transform.Resize(source, newSize, newSize, resizeAlgorithm)
}

// Offset applies the offset values of the layer to the source.
func Offset(icon transfer.RenderIcon, source image.Image, layer transfer.Layer) image.Image {
	if layer.Offset.X == 0 && layer.Offset.Y == 0 {
		return source
	}

	dx := int(math.Round(layer.Offset.X * icon.RenderedScale))
	dy := int(math.Round(layer.Offset.Y * icon.RenderedScale))

	source = clone.Pad(source, icon.RenderedSize, icon.RenderedSize, clone.NoFill)
	source = transform.Translate(source, dx, -dy)
	return source
}

// Expand applies additional padding to the source, so it definitively exceeds the desired size of the icon.
// Note that the image will most likely be too large afterward.
func Expand(icon transfer.RenderIcon, source image.Image, _ transfer.Layer) image.Image {
	if source.Bounds().Size().X >= icon.RenderedSize {
		return source
	}

	return clone.Pad(source, icon.RenderedSize, icon.RenderedSize, clone.NoFill)
}

// Crop will cut out the middle of the source to get the desired size of the icon.
func Crop(icon transfer.RenderIcon, source image.Image, _ transfer.Layer) image.Image {
	if source.Bounds().Size().X <= icon.RenderedSize {
		return source
	}

	position := (source.Bounds().Size().X - icon.RenderedSize) / 2
	return transform.Crop(source, image.Rect(position, position, position+icon.RenderedSize, position+icon.RenderedSize))
}
