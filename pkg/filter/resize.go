package filter

import (
	"github.com/anthonynsimon/bild/transform"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/transfer"
	"image"
)

// ResizeFilter resizes the final image.
type ResizeFilter func(icon transfer.RenderIcon, source image.Image) image.Image

// ResizeToOutput will resize the source to the output size specified in the properties.
func ResizeToOutput(icon transfer.RenderIcon, source image.Image) image.Image {
	if source.Bounds().Size().X == icon.OutputSize {
		return source
	}

	return transform.Resize(source, icon.OutputSize, icon.OutputSize, resizeAlgorithm)
}
