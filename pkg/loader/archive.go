package loader

import (
	"archive/zip"
	"fmt"
	"io"
	"strings"
	"sync"
)

// ModArchiveLoader loads images from a zip archive of the mod.
type ModArchiveLoader struct {
	file      string
	isOpened  bool
	openError error
	files     map[string]*zip.File
	lock      sync.Mutex
}

func NewModArchiveLoader(file string) *ModArchiveLoader {
	return &ModArchiveLoader{
		file:      file,
		isOpened:  false,
		openError: nil,
		files:     make(map[string]*zip.File),
	}
}

func (l *ModArchiveLoader) Load(path string) (io.ReadCloser, error) {
	err := l.open()
	if err != nil {
		return nil, err
	}

	zipFile, ok := l.files[path]
	if !ok {
		return nil, fmt.Errorf("%s: file not found", path)
	}

	return zipFile.Open()
}

func (l *ModArchiveLoader) open() error {
	l.lock.Lock()
	defer l.lock.Unlock()

	if l.isOpened {
		return l.openError
	}

	l.isOpened = true
	l.openError = nil

	zipArchive, err := zip.OpenReader(l.file)
	if err != nil {
		l.openError = err
		return err
	}

	l.files = make(map[string]*zip.File, len(zipArchive.File))
	for _, zipFile := range zipArchive.File {
		if zipFile.FileInfo().IsDir() {
			continue
		}

		parts := strings.SplitN(zipFile.Name, "/", 2)
		if len(parts) > 1 {
			l.files[parts[1]] = zipFile
		}
	}
	return nil
}
