package loader

import (
	"archive/zip"
	"fmt"
	"github.com/stretchr/testify/assert"
	"io"
	"path/filepath"
	"runtime"
	"testing"
)

func TestNewModArchiveLoader(t *testing.T) {
	file := "foo.zip"

	instance := NewModArchiveLoader(file)

	assert.NotNil(t, instance)
	assert.Equal(t, file, instance.file)
	assert.False(t, instance.isOpened)
}

func TestModArchiveLoader_Load(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	file := fmt.Sprintf("%s/../../test/assets/foo_1.2.3.zip", filepath.Dir(filename))

	instance := ModArchiveLoader{
		file:      file,
		isOpened:  false,
		openError: nil,
		files:     make(map[string]*zip.File),
	}

	result, err := instance.Load("test.txt")

	assert.Nil(t, err)
	assert.NotNil(t, result)

	content, _ := io.ReadAll(result)
	_ = result.Close()

	assert.Equal(t, []byte("foo"), content)
}

func TestModArchiveLoader_Load_WithOpenError(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	file := fmt.Sprintf("%s/../../test/assets/foo_1.2.3.zip", filepath.Dir(filename))
	openError := fmt.Errorf("test error")

	instance := ModArchiveLoader{
		file:      file,
		isOpened:  true,
		openError: openError,
		files:     make(map[string]*zip.File),
	}

	_, err := instance.Load("test.txt")

	assert.Equal(t, openError, err)
}

func TestModArchiveLoader_Load_WithLoadError(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	file := fmt.Sprintf("%s/../../test/assets/foo_1.2.3.zip", filepath.Dir(filename))

	instance := ModArchiveLoader{
		file:      file,
		isOpened:  false,
		openError: nil,
		files:     make(map[string]*zip.File),
	}

	_, err := instance.Load("invalid.txt")

	assert.NotNil(t, err)
}

func TestModArchiveLoader_open(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	file := fmt.Sprintf("%s/../../test/assets/foo_1.2.3.zip", filepath.Dir(filename))

	instance := ModArchiveLoader{
		file:      file,
		isOpened:  false,
		openError: nil,
		files:     make(map[string]*zip.File),
	}

	err := instance.open()

	assert.Nil(t, err)
	assert.Len(t, instance.files, 2)
	assert.NotNil(t, instance.files["test.txt"])
	assert.NotNil(t, instance.files["bar/test.txt"])
}

func TestModArchiveLoader_open_WithError(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	file := fmt.Sprintf("%s/../../test/assets/invalid.zip", filepath.Dir(filename))

	instance := ModArchiveLoader{
		file:      file,
		isOpened:  false,
		openError: nil,
		files:     make(map[string]*zip.File),
	}

	err := instance.open()

	assert.NotNil(t, err)
}

func TestModArchiveLoader_open_AlreadyOpened(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	file := fmt.Sprintf("%s/../../test/assets/invalid.zip", filepath.Dir(filename))
	openError := fmt.Errorf("test error")

	instance := ModArchiveLoader{
		file:      file,
		isOpened:  true,
		openError: openError,
		files:     make(map[string]*zip.File),
	}

	err := instance.open()

	assert.Equal(t, openError, err)
}
