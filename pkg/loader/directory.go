package loader

import (
	"fmt"
	"io"
	"os"
)

// ModDirectoryLoader loads images from unzipped mod directories, e.g. from the base or core mod.
type ModDirectoryLoader struct {
	directory string
}

func NewModDirectoryLoader(directory string) *ModDirectoryLoader {
	return &ModDirectoryLoader{
		directory: directory,
	}
}

func (l *ModDirectoryLoader) Load(path string) (io.ReadCloser, error) {
	fp, err := os.Open(fmt.Sprintf("%s/%s", l.directory, path))
	if err != nil {
		return nil, fmt.Errorf("%s: %s", path, err)
	}

	return fp, nil
}
