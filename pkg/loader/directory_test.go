package loader

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"io"
	"path/filepath"
	"runtime"
	"testing"
)

func TestNewModDirectoryLoader(t *testing.T) {
	directory := "foo"

	instance := NewModDirectoryLoader(directory)

	assert.NotNil(t, instance)
	assert.Equal(t, directory, instance.directory)
}

func TestModDirectoryLoader_Load(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	path := "test.txt"

	instance := ModDirectoryLoader{
		directory: fmt.Sprintf("%s/../../test/assets/test", filepath.Dir(filename)),
	}

	result, err := instance.Load(path)

	assert.Nil(t, err)
	assert.NotNil(t, result)

	content, _ := io.ReadAll(result)
	_ = result.Close()

	assert.Equal(t, []byte("foo"), content)
}

func TestModDirectoryLoader_Load_WithError(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	path := "invalid.txt"

	instance := ModDirectoryLoader{
		directory: fmt.Sprintf("%s/../../test/assets", filepath.Dir(filename)),
	}

	_, err := instance.Load(path)

	assert.NotNil(t, err)
}
