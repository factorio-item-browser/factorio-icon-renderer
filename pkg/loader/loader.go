package loader

import (
	"fmt"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/errors"
	"image"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"io"
	"os"
	"regexp"
	"strings"
)

var regexpModVersion = regexp.MustCompile(`_[\d.]+$`)
var regexpImageFileName = regexp.MustCompile(`^__(.*)__/(.*)$`)

type modLoader interface {
	Load(path string) (io.ReadCloser, error)
}

type Loader struct {
	mods map[string]modLoader
}

func New() *Loader {
	return &Loader{
		mods: make(map[string]modLoader),
	}
}

func (l *Loader) AddModsDirectory(directory string) {
	files, err := os.ReadDir(directory)
	if err != nil {
		return
	}

	for _, file := range files {
		fullPath := fmt.Sprintf("%s/%s", directory, file.Name())
		modName := file.Name()
		isZip := strings.HasSuffix(modName, ".zip")
		if isZip {
			modName = modName[0 : len(modName)-4]
		}

		modVersion := regexpModVersion.FindString(modName)
		modName = modName[0 : len(modName)-len(modVersion)]

		if file.IsDir() {
			l.mods[modName] = NewModDirectoryLoader(fullPath)
			continue
		}

		if isZip {
			l.mods[modName] = NewModArchiveLoader(fullPath)
			continue
		}
	}
}

func (l *Loader) Load(fileName string) (image.Image, error) {
	modName, imagePath, ok := splitImageFileName(fileName)
	if !ok {
		return nil, errors.NewInvalidImageFileNameError(fileName)
	}

	modLoader, ok := l.mods[modName]
	if !ok {
		return nil, errors.NewUnknownModError(modName)
	}

	fp, err := modLoader.Load(imagePath)
	if err != nil {
		return nil, errors.NewLoadImageError(modName, imagePath, err)
	}
	defer func() {
		_ = fp.Close()
	}()

	img, _, err := image.Decode(fp)
	if err != nil {
		return nil, errors.NewLoadImageError(modName, imagePath, err)
	}
	return img, nil
}

// splitImageFileName splits the provided image fileName into the mod, and the mod-internal file. The third return
// value provides whether splitting the fileName was actually successful.
func splitImageFileName(fileName string) (string, string, bool) {
	match := regexpImageFileName.FindStringSubmatch(fileName)
	if match == nil {
		return "", "", false
	}

	return match[1], match[2], true
}
