package loader

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/errors"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/test/mocks"
	"image"
	"os"
	"path/filepath"
	"runtime"
	"testing"
)

func TestNew(t *testing.T) {
	instance := New()

	assert.NotNil(t, instance)
	assert.NotNil(t, instance.mods)
}

func TestLoader_AddModsDirectory(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	directory := fmt.Sprintf("%s/../../test/assets", filepath.Dir(filename))

	instance := Loader{
		mods: make(map[string]modLoader),
	}

	instance.AddModsDirectory(directory)

	assert.Len(t, instance.mods, 3)
	assert.IsType(t, &ModDirectoryLoader{}, instance.mods["test"])
	assert.IsType(t, &ModArchiveLoader{}, instance.mods["foo"])
	assert.IsType(t, &ModArchiveLoader{}, instance.mods["invalid"])
}

func TestLoader_AddModsDirectory_WithError(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	directory := fmt.Sprintf("%s/../../test/invalid", filepath.Dir(filename))

	instance := Loader{
		mods: make(map[string]modLoader),
	}

	instance.AddModsDirectory(directory)

	assert.Len(t, instance.mods, 0)
}

func TestLoader_Load(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	fp, _ := os.Open(fmt.Sprintf("%s/../../test/assets/test.png", filepath.Dir(filename)))

	fileName := "__test__/test.png"
	expectedResult := image.RGBA{
		Pix: []uint8{
			0xFF, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 0xFF,
			0x00, 0xFF, 0x00, 0xFF, 0x00, 0x00, 0xFF, 0xFF,
		},
		Stride: 8,
		Rect:   image.Rect(0, 0, 2, 2),
	}

	testLoader := mocks.NewModLoader(t)
	testLoader.On("Load", "test.png").Once().Return(fp, nil)

	instance := Loader{
		mods: map[string]modLoader{
			"test": testLoader,
		},
	}

	result, err := instance.Load(fileName)

	assert.Nil(t, err)
	assert.Equal(t, &expectedResult, result)
}

func TestLoader_Load_WithInvalidFileNameError(t *testing.T) {
	fileName := "invalid"
	testLoader := mocks.NewModLoader(t)

	instance := Loader{
		mods: map[string]modLoader{
			"test": testLoader,
		},
	}

	_, err := instance.Load(fileName)

	assert.IsType(t, &errors.InvalidImageFileNameError{}, err)
}

func TestLoader_Load_WithUnknownModError(t *testing.T) {
	fileName := "__unknown__/test.png"
	testLoader := mocks.NewModLoader(t)

	instance := Loader{
		mods: map[string]modLoader{
			"test": testLoader,
		},
	}

	_, err := instance.Load(fileName)

	assert.IsType(t, &errors.UnknownModError{}, err)
}

func TestLoader_Load_WithImageLoadError(t *testing.T) {
	fileName := "__test__/test.png"

	testLoader := mocks.NewModLoader(t)
	testLoader.On("Load", "test.png").Once().Return(nil, fmt.Errorf("test error"))

	instance := Loader{
		mods: map[string]modLoader{
			"test": testLoader,
		},
	}

	_, err := instance.Load(fileName)

	assert.IsType(t, &errors.LoadImageError{}, err)
}

func TestLoader_Load_WithImageDecodeError(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	fp, _ := os.Open(fmt.Sprintf("%s/../../test/assets/invalid.zip", filepath.Dir(filename)))

	fileName := "__test__/test.png"

	testLoader := mocks.NewModLoader(t)
	testLoader.On("Load", "test.png").Once().Return(fp, nil)

	instance := Loader{
		mods: map[string]modLoader{
			"test": testLoader,
		},
	}

	_, err := instance.Load(fileName)

	assert.IsType(t, &errors.LoadImageError{}, err)
}
