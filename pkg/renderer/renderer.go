package renderer

import (
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/errors"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/filter"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/loader"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/transfer"
	"image"
)

// RenderFilter holds the filters to be used during rendering an icon.
type RenderFilter struct {
	Create filter.CreateFilter
	Layer  filter.LayerFilter
	Blend  filter.BlendFilter
	Resize filter.ResizeFilter
}

// defaultFilter are the actual filter functions to use for rendering.
var defaultFilter = RenderFilter{
	Create: filter.Create,
	Layer: filter.ChainLayerFilters(
		filter.RemoveMipMaps,
		filter.Scale,
		filter.Offset,
		filter.Expand,
		filter.Crop,
	),
	Blend:  filter.TintedBlendFilter,
	Resize: filter.ResizeToOutput,
}

type imageLoader interface {
	AddModsDirectory(string)
	Load(string) (image.Image, error)
}

type Renderer struct {
	filter RenderFilter
	loader imageLoader
}

func New() *Renderer {
	instance := Renderer{
		filter: defaultFilter,
		loader: loader.New(),
	}

	return &instance
}

// AddModsDirectory adds a directory containing the mods to the renderer. It is possible to add several directories.
func (r *Renderer) AddModsDirectory(modsDirectory string) {
	r.loader.AddModsDirectory(modsDirectory)
}

// Render renders the provided icon into the final image.
func (r *Renderer) Render(icon transfer.RenderIcon) (image.Image, error) {
	iconImage := r.filter.Create(icon.RenderedSize)
	for _, layer := range icon.Layers {
		layerImage, err := r.loader.Load(layer.FileName)
		if err != nil {
			return nil, errors.NewRenderError(icon, err)
		}

		layerImage = r.filter.Layer(icon, layerImage, layer)
		iconImage = r.filter.Blend(icon, iconImage, layerImage, layer)
	}
	iconImage = r.filter.Resize(icon, iconImage)

	return iconImage, nil
}
