package renderer

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/errors"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/filter"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/transfer"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/test/mocks"
	"image"
	"testing"
)

func TestNew(t *testing.T) {
	instance := New()

	assert.NotNil(t, instance)
	assert.NotNil(t, instance.loader)
}

func TestRenderer_AddModsDirectory(t *testing.T) {
	modsDirectory := "foo"

	loader := mocks.NewImageLoader(t)
	loader.On("AddModsDirectory", modsDirectory).Once()

	instance := Renderer{
		loader: loader,
	}
	instance.AddModsDirectory(modsDirectory)
}

func TestFactorioIconRenderer_Render(t *testing.T) {
	layer1 := transfer.Layer{
		FileName: "__abc__/def",
		Size:     64,
		Scale:    1,
	}
	layer2 := transfer.Layer{
		FileName: "__ghi__/jkl",
	}
	icon := transfer.RenderIcon{
		RenderedSize: 64,
		Layers:       []transfer.Layer{layer1, layer2},
	}

	image1 := image.NewRGBA(image.Rect(0, 0, 1, 1))
	image2 := image.NewRGBA(image.Rect(0, 0, 1, 2))
	image3 := image.NewRGBA(image.Rect(0, 0, 1, 3))
	image4 := image.NewRGBA(image.Rect(0, 0, 1, 4))
	image5 := image.NewRGBA(image.Rect(0, 0, 1, 5))
	image6 := image.NewRGBA(image.Rect(0, 0, 1, 6))
	image7 := image.NewRGBA(image.Rect(0, 0, 1, 7))
	image8 := image.NewRGBA(image.Rect(0, 0, 1, 8))

	imageLoader := mocks.NewImageLoader(t)
	imageLoader.On("Load", "__abc__/def").Once().Return(image2, nil)
	imageLoader.On("Load", "__ghi__/jkl").Once().Return(image3, nil)

	create := func(size int) image.Image {
		assert.Equal(t, 64, size)
		return image1
	}
	layer := func(ri transfer.RenderIcon, i image.Image, l transfer.Layer) image.Image {
		assert.Equal(t, icon, ri)
		if l == layer1 {
			assert.Equal(t, image2, i)
			return image4
		}
		if l == layer2 {
			assert.Equal(t, image3, i)
			return image5
		}
		return nil
	}
	blend := func(ri transfer.RenderIcon, i1, i2 image.Image, l transfer.Layer) image.Image {
		assert.Equal(t, icon, ri)
		if l == layer1 {
			assert.Equal(t, image1, i1)
			assert.Equal(t, image4, i2)
			return image6
		}
		if l == layer2 {
			assert.Equal(t, image6, i1)
			assert.Equal(t, image5, i2)
			return image7
		}
		return nil
	}
	resize := func(ri transfer.RenderIcon, i image.Image) image.Image {
		assert.Equal(t, icon, ri)
		assert.Equal(t, image7, i)
		return image8
	}

	instance := Renderer{
		filter: RenderFilter{
			Create: create,
			Layer:  layer,
			Blend:  blend,
			Resize: resize,
		},
		loader: imageLoader,
	}
	result, err := instance.Render(icon)

	assert.Nil(t, err)
	assert.Equal(t, image8, result)
}

func TestFactorioIconRenderer_Render_WithLoadError(t *testing.T) {
	layer := transfer.Layer{
		FileName: "__abc__/def",
		Size:     64,
		Scale:    1,
	}
	icon := transfer.RenderIcon{
		RenderedSize: 64,
		Layers:       []transfer.Layer{layer},
	}

	imageLoader := mocks.NewImageLoader(t)
	imageLoader.On("Load", "__abc__/def").Once().Return(nil, fmt.Errorf("test error"))

	instance := Renderer{
		filter: RenderFilter{
			Create: filter.Create,
			Layer: filter.ChainLayerFilters(
				filter.RemoveMipMaps,
				filter.Scale,
				filter.Offset,
				filter.Expand,
				filter.Crop,
			),
			Blend:  filter.TintedBlendFilter,
			Resize: filter.ResizeToOutput,
		},
		loader: imageLoader,
	}
	_, err := instance.Render(icon)

	assert.IsType(t, &errors.RenderError{}, err)
}
