package transfer

import (
	"encoding/json"
)

type Color struct {
	Red   float64 `json:"r"`
	Green float64 `json:"g"`
	Blue  float64 `json:"b"`
	Alpha float64 `json:"a"`
}

func (c *Color) UnmarshalJSON(data []byte) error {
	type color Color
	temp := color{0, 0, 0, 1}

	err := json.Unmarshal(data, &temp)
	if err != nil {
		err = json.Unmarshal(data, &[]any{&temp.Red, &temp.Green, &temp.Blue, &temp.Alpha})
	}
	if err != nil {
		return err
	}

	c.Red = convertColorValue(temp.Red)
	c.Green = convertColorValue(temp.Green)
	c.Blue = convertColorValue(temp.Blue)
	c.Alpha = convertColorValue(temp.Alpha)
	return nil
}

func convertColorValue(v float64) float64 {
	if v > 1 {
		return v / 255.
	}
	return v
}

type Vector struct {
	X float64
	Y float64
}

func (v *Vector) UnmarshalJSON(data []byte) error {
	return json.Unmarshal(data, &[]any{&v.X, &v.Y})
}

type IconData struct {
	Icon        string   `json:"icon"`
	IconSize    int16    `json:"icon_size"`
	Tint        *Color   `json:"tint"`
	Shift       *Vector  `json:"shift"`
	Scale       *float64 `json:"scale"`
	IconMipmaps uint8    `json:"icon_mipmaps"`
}

type FactorioIcon struct {
	Icon        string     `json:"icon"`
	Icons       []IconData `json:"icons"`
	IconSize    int16      `json:"icon_size"`
	IconMipmaps uint8      `json:"icon_mipmaps"`
}
