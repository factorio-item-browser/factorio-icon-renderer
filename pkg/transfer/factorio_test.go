package transfer

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestColor_UnmarshalJSON(t *testing.T) {
	tests := map[string]struct {
		data           string
		expectError    bool
		expectedResult Color
	}{
		"object": {
			data:        `{"r":0.1, "g":0.2, "b":0.3, "a":0.4}`,
			expectError: false,
			expectedResult: Color{
				Red:   0.1,
				Green: 0.2,
				Blue:  0.3,
				Alpha: 0.4,
			},
		},
		"object default values": {
			data:        `{}`,
			expectError: false,
			expectedResult: Color{
				Red:   0,
				Green: 0,
				Blue:  0,
				Alpha: 1,
			},
		},
		"object convert values": {
			data:        `{"r":25.5, "g":51, "b":76.5, "a":102}`,
			expectError: false,
			expectedResult: Color{
				Red:   0.1,
				Green: 0.2,
				Blue:  0.3,
				Alpha: 0.4,
			},
		},
		"array": {
			data:        `[0.1, 0.2, 0.3, 0.4]`,
			expectError: false,
			expectedResult: Color{
				Red:   0.1,
				Green: 0.2,
				Blue:  0.3,
				Alpha: 0.4,
			},
		},
		"array default values": {
			data:        `[]`,
			expectError: false,
			expectedResult: Color{
				Red:   0,
				Green: 0,
				Blue:  0,
				Alpha: 1,
			},
		},
		"array convert values": {
			data:        `[25.5, 51, 76.5, 102]`,
			expectError: false,
			expectedResult: Color{
				Red:   0.1,
				Green: 0.2,
				Blue:  0.3,
				Alpha: 0.4,
			},
		},
		"invalid json": {
			data:           `["invalid"]`,
			expectError:    true,
			expectedResult: Color{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			var instance Color
			err := json.Unmarshal([]byte(test.data), &instance)

			if test.expectError {
				assert.NotNil(t, err)
			} else {
				assert.Nil(t, err)
				assert.Equal(t, test.expectedResult, instance)
			}
		})
	}
}

func TestFactorioIcon_Unmarshal(t *testing.T) {
	scale := 0.25

	tests := map[string]struct {
		data           string
		expectedResult FactorioIcon
	}{
		"simple": {
			data: `{"type":"item","name":"electronic-circuit","icon":"__base__/graphics/icons/electronic-circuit.png","icon_size":64,"icon_mipmaps":4}`,
			expectedResult: FactorioIcon{
				Icon:        "__base__/graphics/icons/electronic-circuit.png",
				IconSize:    64,
				IconMipmaps: 4,
			},
		},
		"fill barrel": {
			data: `{"type":"recipe","name":"fill-water-barrel","icons":[{"icon":"__base__/graphics/icons/fluid/barreling/barrel-fill.png","icon_size":64,"icon_mipmaps":4},{"icon":"__base__/graphics/icons/fluid/barreling/barrel-fill-side-mask.png","icon_size":64,"icon_mipmaps":4,"tint":{"r":0,"g":0.34,"b":0.6,"a":0.75}},{"icon":"__base__/graphics/icons/fluid/barreling/barrel-fill-top-mask.png","icon_size":64,"icon_mipmaps":4,"tint":{"r":0.7,"g":0.7,"b":0.7,"a":0.75}},{"icon":"__base__/graphics/icons/fluid/water.png","icon_size":64,"icon_mipmaps":4,"scale":0.25,"shift":[-8,-8]}],"icon_size":64,"icon_mipmaps":4}`,
			expectedResult: FactorioIcon{
				Icons: []IconData{
					{
						Icon:        "__base__/graphics/icons/fluid/barreling/barrel-fill.png",
						IconSize:    64,
						IconMipmaps: 4,
					},
					{
						Icon:        "__base__/graphics/icons/fluid/barreling/barrel-fill-side-mask.png",
						IconSize:    64,
						IconMipmaps: 4,
						Tint: &Color{
							Red:   0,
							Green: 0.34,
							Blue:  0.6,
							Alpha: 0.75,
						},
					},
					{
						Icon:        "__base__/graphics/icons/fluid/barreling/barrel-fill-top-mask.png",
						IconSize:    64,
						IconMipmaps: 4,
						Tint: &Color{
							Red:   0.7,
							Green: 0.7,
							Blue:  0.7,
							Alpha: 0.75,
						},
					},
					{
						Icon:     "__base__/graphics/icons/fluid/water.png",
						IconSize: 64,
						Shift: &Vector{
							X: -8,
							Y: -8,
						},
						Scale:       &scale,
						IconMipmaps: 4,
					},
				},
				IconSize:    64,
				IconMipmaps: 4,
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			var result FactorioIcon
			err := json.Unmarshal([]byte(test.data), &result)

			assert.Nil(t, err)
			assert.Equal(t, test.expectedResult, result)
		})
	}
}
