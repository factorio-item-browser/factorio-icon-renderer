package transfer

type ScaleBase uint

const (
	// ScaleBaseDefault is the scale base used for most of the icons. The first layer of the icon will set the scale.
	ScaleBaseDefault ScaleBase = 0
	// ScaleBaseItemOrRecipe is the scale base used for item and recipe icons. The scale is always based on 32px.
	ScaleBaseItemOrRecipe ScaleBase = 32
)

type Layer struct {
	FileName string
	Offset   Vector
	Scale    float64
	Size     int
	Tint     Color
}

type RenderIcon struct {
	Layers        []Layer
	OutputSize    int
	RenderedSize  int
	RenderedScale float64
}

// NewRenderIcon transforms the provided Factorio icon into the structure required by the actual renderer, adjusting
// the sizes etc. of the layers to match what Factorio would actually render.
// The scaleBase is the size of the icon on which the scale values are based. If an item or recipe icon is rendered,
// the scaleBase must be 32. For all other types of icons, 0 must be passed so that scaling is based on the first layer.
// The sizeToRender is the size the final image should have. The renderer will scale the rendered icon accordingly.
func NewRenderIcon(factorioIcon FactorioIcon, scaleBase ScaleBase, sizeToRender int) RenderIcon {
	// In Factorio 2.x, icon_size is optional. If omitted, 64px is assumed as default.
	if factorioIcon.IconSize == 0 && (len(factorioIcon.Icons) == 0 || factorioIcon.Icons[0].IconSize == 0) {
		factorioIcon.IconSize = 64
	}

	// Convert simple definition to layer definition
	if len(factorioIcon.Icons) == 0 && factorioIcon.Icon != "" {
		factorioIcon.Icons = []IconData{
			{
				Icon:        factorioIcon.Icon,
				IconSize:    factorioIcon.IconSize,
				IconMipmaps: factorioIcon.IconMipmaps,
			},
		}
	}

	renderIcon := RenderIcon{
		Layers:        make([]Layer, 0, len(factorioIcon.Icons)),
		OutputSize:    sizeToRender,
		RenderedSize:  sizeToRender,
		RenderedScale: 1.,
	}

	firstLayerSize := 32
	if factorioIcon.IconSize > 0 {
		firstLayerSize = int(factorioIcon.IconSize)
	}
	if len(factorioIcon.Icons) > 0 && factorioIcon.Icons[0].IconSize > 0 {
		firstLayerSize = int(factorioIcon.Icons[0].IconSize)
	}

	firstLayerScale := 1.
	if scaleBase > 0 {
		firstLayerScale = float64(scaleBase) / float64(firstLayerSize)
	}
	if len(factorioIcon.Icons) > 0 && factorioIcon.Icons[0].Scale != nil {
		firstLayerScale = *factorioIcon.Icons[0].Scale
	}

	for _, icon := range factorioIcon.Icons {
		layerSize := factorioIcon.IconSize
		if icon.IconSize > 0 {
			layerSize = icon.IconSize
		}

		ratio := 1. / firstLayerScale

		scale := 1.
		if scaleBase > 0 {
			scale = float64(scaleBase) / float64(layerSize)
		}
		if icon.Scale != nil {
			scale = *icon.Scale
		}

		layer := Layer{
			FileName: icon.Icon,
			Size:     int(layerSize),
			Scale:    scale * ratio,
		}

		if icon.Shift != nil {
			layer.Offset = Vector{
				X: icon.Shift.X * ratio,
				Y: icon.Shift.Y * ratio,
			}
		}

		if icon.Tint == nil {
			layer.Tint = Color{1, 1, 1, 1}
		} else {
			layer.Tint = *icon.Tint
		}

		renderIcon.Layers = append(renderIcon.Layers, layer)
	}

	if len(renderIcon.Layers) > 0 && renderIcon.Layers[0].Size > 0 {
		renderIcon.RenderedSize = int(float64(renderIcon.Layers[0].Size) * renderIcon.Layers[0].Scale)
	}

	if renderIcon.RenderedSize < renderIcon.OutputSize {
		// We would enlarge the icon in the end, so render it bigger to begin with.
		renderIcon.RenderedScale = float64(renderIcon.OutputSize) / float64(renderIcon.RenderedSize)
		renderIcon.RenderedSize = renderIcon.OutputSize
	}

	return renderIcon
}
