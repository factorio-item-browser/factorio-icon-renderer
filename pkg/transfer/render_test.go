package transfer

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func ptr[T any](t T) *T {
	return &t
}

func TestNewRenderIcon(t *testing.T) {
	scale := 0.25
	defaultOffset := Vector{}
	defaultTint := Color{
		Red:   1,
		Green: 1,
		Blue:  1,
		Alpha: 1,
	}

	tests := map[string]struct {
		factorioIcon   FactorioIcon
		scaleBase      ScaleBase
		sizeToRender   int
		expectedResult RenderIcon
	}{
		"simple": {
			factorioIcon: FactorioIcon{
				Icon:        "__base__/graphics/icons/electronic-circuit.png",
				IconSize:    64,
				IconMipmaps: 4,
			},
			scaleBase:    ScaleBaseItemOrRecipe,
			sizeToRender: 64,
			expectedResult: RenderIcon{
				Layers: []Layer{
					{
						FileName: "__base__/graphics/icons/electronic-circuit.png",
						Offset:   defaultOffset,
						Scale:    1,
						Size:     64,
						Tint:     defaultTint,
					},
				},
				OutputSize:    64,
				RenderedSize:  64,
				RenderedScale: 1,
			},
		},
		"without icon size": {
			factorioIcon: FactorioIcon{
				Icon:        "__base__/graphics/icons/electronic-circuit.png",
				IconMipmaps: 4,
			},
			scaleBase:    ScaleBaseItemOrRecipe,
			sizeToRender: 64,
			expectedResult: RenderIcon{
				Layers: []Layer{
					{
						FileName: "__base__/graphics/icons/electronic-circuit.png",
						Offset:   defaultOffset,
						Scale:    1,
						Size:     64,
						Tint:     defaultTint,
					},
				},
				OutputSize:    64,
				RenderedSize:  64,
				RenderedScale: 1,
			},
		},
		"fill barrel": {
			factorioIcon: FactorioIcon{
				Icons: []IconData{
					{
						Icon:        "__base__/graphics/icons/fluid/barreling/barrel-fill.png",
						IconSize:    64,
						IconMipmaps: 4,
					},
					{
						Icon:        "__base__/graphics/icons/fluid/barreling/barrel-fill-side-mask.png",
						IconSize:    64,
						IconMipmaps: 4,
						Tint: &Color{
							Red:   0,
							Green: 0.34,
							Blue:  0.6,
							Alpha: 0.75,
						},
					},
					{
						Icon:        "__base__/graphics/icons/fluid/barreling/barrel-fill-top-mask.png",
						IconSize:    64,
						IconMipmaps: 4,
						Tint: &Color{
							Red:   0.7,
							Green: 0.7,
							Blue:  0.7,
							Alpha: 0.75,
						},
					},
					{
						Icon:     "__base__/graphics/icons/fluid/water.png",
						IconSize: 64,
						Shift: &Vector{
							X: -8,
							Y: -8,
						},
						Scale:       &scale,
						IconMipmaps: 4,
					},
				},
				IconSize:    64,
				IconMipmaps: 4,
			},
			scaleBase:    ScaleBaseItemOrRecipe,
			sizeToRender: 64,
			expectedResult: RenderIcon{
				Layers: []Layer{
					{
						FileName: "__base__/graphics/icons/fluid/barreling/barrel-fill.png",
						Offset:   defaultOffset,
						Scale:    1,
						Size:     64,
						Tint:     defaultTint,
					},
					{
						FileName: "__base__/graphics/icons/fluid/barreling/barrel-fill-side-mask.png",
						Offset:   defaultOffset,
						Scale:    1,
						Size:     64,
						Tint: Color{
							Red:   0,
							Green: 0.34,
							Blue:  0.6,
							Alpha: 0.75,
						},
					},
					{
						FileName: "__base__/graphics/icons/fluid/barreling/barrel-fill-top-mask.png",
						Offset:   defaultOffset,
						Scale:    1,
						Size:     64,
						Tint: Color{
							Red:   0.7,
							Green: 0.7,
							Blue:  0.7,
							Alpha: 0.75,
						},
					},
					{
						FileName: "__base__/graphics/icons/fluid/water.png",
						Offset: Vector{
							X: -16,
							Y: -16,
						},
						Scale: 0.5,
						Size:  64,
						Tint:  defaultTint,
					},
				},
				OutputSize:    64,
				RenderedSize:  64,
				RenderedScale: 1,
			},
		},
		"oversized": {
			factorioIcon: FactorioIcon{
				Icon:        "__base__/graphics/icons/electronic-circuit.png",
				IconSize:    64,
				IconMipmaps: 4,
			},
			scaleBase:    ScaleBaseItemOrRecipe,
			sizeToRender: 256,
			expectedResult: RenderIcon{
				Layers: []Layer{
					{
						FileName: "__base__/graphics/icons/electronic-circuit.png",
						Offset:   defaultOffset,
						Scale:    1,
						Size:     64,
						Tint:     defaultTint,
					},
				},
				OutputSize:    256,
				RenderedSize:  256,
				RenderedScale: 4,
			},
		},
		"first layer scale": {
			factorioIcon: FactorioIcon{
				Icons: []IconData{
					{
						Icon:     "__debug-icons__/graphics/64.png",
						IconSize: 64,
						Scale:    ptr(1.),
					},
					{
						Icon:     "__debug-icons__/graphics/64.png",
						IconSize: 64,
						Scale:    ptr(0.5),
						Shift: &Vector{
							X: 16,
							Y: 16,
						},
					},
				},
			},
			scaleBase:    ScaleBaseItemOrRecipe,
			sizeToRender: 64,
			expectedResult: RenderIcon{
				Layers: []Layer{
					{
						FileName: "__debug-icons__/graphics/64.png",
						Offset:   defaultOffset,
						Scale:    1,
						Size:     64,
						Tint:     defaultTint,
					},
					{
						FileName: "__debug-icons__/graphics/64.png",
						Offset: Vector{
							X: 16,
							Y: 16,
						},
						Scale: 0.5,
						Size:  64,
						Tint:  defaultTint,
					},
				},
				OutputSize:    64,
				RenderedSize:  64,
				RenderedScale: 1,
			},
		},
		"electric-energy-interface": {
			factorioIcon: FactorioIcon{
				Icons: []IconData{
					{
						Icon: "__base__/graphics/icons/accumulator.png",
						Tint: &Color{
							Red:   1,
							Green: 0.8,
							Blue:  1,
							Alpha: 1,
						},
					},
				},
				IconSize:    64,
				IconMipmaps: 4,
			},
			scaleBase:    ScaleBaseItemOrRecipe,
			sizeToRender: 64,
			expectedResult: RenderIcon{
				Layers: []Layer{
					{
						FileName: "__base__/graphics/icons/accumulator.png",
						Offset:   defaultOffset,
						Scale:    1,
						Size:     64,
						Tint: Color{
							Red:   1,
							Green: 0.8,
							Blue:  1,
							Alpha: 1,
						},
					},
				},
				OutputSize:    64,
				RenderedSize:  64,
				RenderedScale: 1,
			},
		},
		"cube-cyclotron-uranium": {
			factorioIcon: FactorioIcon{
				Icons: []IconData{
					{
						IconSize:    256,
						IconMipmaps: 4,
						Icon:        "__Ultracube__/assets/technology/cyclotron.png",
					},
					{
						IconSize:    256,
						IconMipmaps: 4,
						Icon:        "__Krastorio2Assets__/technologies/matter-uranium.png",
						Scale:       ptr(0.625),
					},
				},
			},
			scaleBase:    ScaleBaseDefault,
			sizeToRender: 64,
			expectedResult: RenderIcon{
				Layers: []Layer{
					{
						FileName: "__Ultracube__/assets/technology/cyclotron.png",
						Offset:   defaultOffset,
						Scale:    1,
						Size:     256,
						Tint:     defaultTint,
					},
					{
						FileName: "__Krastorio2Assets__/technologies/matter-uranium.png",
						Offset:   defaultOffset,
						Scale:    0.625,
						Size:     256,
						Tint:     defaultTint,
					},
				},
				OutputSize:    64,
				RenderedSize:  256,
				RenderedScale: 1,
			},
		},
		"cube-matter-replication": {
			factorioIcon: FactorioIcon{
				Icons: []IconData{
					{
						IconSize:    128,
						IconMipmaps: 4,
						Icon:        "__Krastorio2Assets__/icons/entities/matter-assembler.png",
					},
					{
						IconSize:    64,
						IconMipmaps: 4,
						Icon:        "__Krastorio2Assets__/icons/fluids/hydrogen.png",
						Scale:       ptr(1.),
						Shift: &Vector{
							X: 24,
							Y: 24,
						},
					},
				},
			},
			scaleBase:    ScaleBaseDefault,
			sizeToRender: 64,
			expectedResult: RenderIcon{
				Layers: []Layer{
					{
						FileName: "__Krastorio2Assets__/icons/entities/matter-assembler.png",
						Offset:   defaultOffset,
						Scale:    1,
						Size:     128,
						Tint:     defaultTint,
					},
					{
						FileName: "__Krastorio2Assets__/icons/fluids/hydrogen.png",
						Offset: Vector{
							X: 24,
							Y: 24,
						},
						Scale: 1,
						Size:  64,
						Tint:  defaultTint,
					},
				},
				OutputSize:    64,
				RenderedSize:  128,
				RenderedScale: 1,
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			result := NewRenderIcon(test.factorioIcon, test.scaleBase, test.sizeToRender)

			assert.Equal(t, test.expectedResult, result)
		})
	}
}
